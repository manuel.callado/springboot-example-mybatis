package com.example.demo.controller;

import com.example.demo.business.BankBusiness;
import com.example.demo.repository.entity.BankEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/banks")
public class GetExampleController {

    @Autowired
    BankBusiness business;

    @RequestMapping(method = RequestMethod.GET)
    public List<BankEntity> getBanks() {
        return business.getAllBank();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BankEntity getBanksProvider(@PathVariable("id") String idRest) {

        System.out.println(idRest);

        return business.getBannkById(idRest);
    }

}
