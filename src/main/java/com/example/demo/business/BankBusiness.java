package com.example.demo.business;

import com.example.demo.repository.BankRepository;
import com.example.demo.repository.entity.BankEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BankBusiness {

    @Autowired
    BankRepository bankRepository;

    public List<BankEntity> getAllBank() {

       return bankRepository.getBanks();

    }

    public BankEntity getBannkById(String id) {

        return bankRepository.getBankById(id);

    }





}
