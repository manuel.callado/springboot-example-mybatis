package com.example.demo.repository.entity;

public class BankEntity {

    String description;
    String id;

    public BankEntity() {

    }
    public BankEntity(String description, String id) {
        this.description = description;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
