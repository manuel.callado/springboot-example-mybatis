package com.example.demo.repository;

import com.example.demo.repository.entity.BankEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface BankRepository {

    @Select("select * from banks")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "description", column = "DESCRIPTION")
    })
    public List<BankEntity> getBanks();



    @Select("select * from banks where id = #{bankId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "description", column = "DESCRIPTION")
    })
    public BankEntity getBankById(@Param("bankId") String bankId);

}
