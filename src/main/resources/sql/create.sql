CREATE TABLE banks (
         id      VARCHAR2(20) PRIMARY KEY,
         description VARCHAR2(120) NOT NULL);

COMMENT ON TABLE banks IS 'Table of Banks';

INSERT INTO banks (id,description) VALUES ('ING', 'Soy ahorrador');